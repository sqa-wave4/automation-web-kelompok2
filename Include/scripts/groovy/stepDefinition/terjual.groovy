package stepDefinition

import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows

import internal.GlobalVariable

import cucumber.api.java.en.And
import cucumber.api.java.en.Given
import cucumber.api.java.en.Then
import cucumber.api.java.en.When

public class terjual {

	@Given("User already on Terjual page")
	public void user_already_on_Terjual_page() {
		WebUI.callTestCase(findTestCase('Pages/Page Daftar Jual Saya/Click Terjual Page'), [:], FailureHandling.STOP_ON_FAILURE)
	}

	@When("User go to the detail produk page")
	public void User_go_to_the_detail_produk_page() {
		WebUI.callTestCase(findTestCase('Pages/Page Terjual/Go to the Detail Produk Terjual'), [:], FailureHandling.STOP_ON_FAILURE)
	}
	
	@Then("User is in the product details")
	public void User_is_in_the_product_details() {
		WebUI.callTestCase(findTestCase('Pages/Page Daftar Jual Saya/_Page Terjual/Click Detail Product - Terjual'), [:], FailureHandling.STOP_ON_FAILURE)
	}
}
