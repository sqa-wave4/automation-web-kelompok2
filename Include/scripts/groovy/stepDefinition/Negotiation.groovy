package stepDefinition
import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testcase.TestCaseFactory
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testdata.TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI

import internal.GlobalVariable

import org.openqa.selenium.WebElement
import org.openqa.selenium.WebDriver
import org.openqa.selenium.By

import com.kms.katalon.core.mobile.keyword.internal.MobileDriverFactory
import com.kms.katalon.core.webui.driver.DriverFactory

import com.kms.katalon.core.testobject.RequestObject
import com.kms.katalon.core.testobject.ResponseObject
import com.kms.katalon.core.testobject.ConditionType
import com.kms.katalon.core.testobject.TestObjectProperty

import com.kms.katalon.core.mobile.helper.MobileElementCommonHelper
import com.kms.katalon.core.util.KeywordUtil

import com.kms.katalon.core.webui.exception.WebElementNotFoundException

import cucumber.api.java.en.And
import cucumber.api.java.en.Given
import cucumber.api.java.en.Then
import cucumber.api.java.en.When



class Negotiation {
	@Given("user choose the product")
	public void user_choose_the_product() {
		WebUI.callTestCase(findTestCase('Pages/Negotiation/Choose Product'), [:], FailureHandling.STOP_ON_FAILURE)
	}

	@When("the product detail is displayed")
	public void the_product_detail_is_displayed() {
		WebUI.callTestCase(findTestCase('Pages/Negotiation/Navigate to Negotiation Page'), [:], FailureHandling.STOP_ON_FAILURE)
	}

	@Then("user click the Saya Tertarik button")
	public void user_click_the_Saya_Tertarik_button() {
		WebUI.callTestCase(findTestCase('Pages/Negotiation/Click Saya Tertarik'), [:], FailureHandling.STOP_ON_FAILURE)
	}

	@Then("user click the Kirim button")
	public void user_click_the_Kirim_button() {
		WebUI.callTestCase(findTestCase('Pages/Negotiation/Submit negotiation to seller'), [:], FailureHandling.STOP_ON_FAILURE)
	}

	@Then("user input the price {string}")
	public void user_input_the_price(String string) {
		WebUI.callTestCase(findTestCase('Pages/Negotiation/Input the price'), [:], FailureHandling.STOP_ON_FAILURE)
	}
}