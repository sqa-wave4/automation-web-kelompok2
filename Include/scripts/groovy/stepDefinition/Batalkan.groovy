package stepDefinition

import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows

import internal.GlobalVariable

import cucumber.api.java.en.And
import cucumber.api.java.en.Given
import cucumber.api.java.en.Then
import cucumber.api.java.en.When

public class Batalkan {

	@Given("User click Logo to go Homepage")
	public void user_click_Logo_to_go_Homepage() {
		WebUI.callTestCase(findTestCase('Pages/Homepage/Click Logo'), [:], FailureHandling.STOP_ON_FAILURE)
	}

	@When("User wants to go Daftar Jual Saya page from Homepage")
	public void user_wants_to_go_Daftar_Jual_Saya_page_from_Homepage() {
		WebUI.callTestCase(findTestCase('Pages/Homepage/Click Button Icon Daftar Jual'), [:], FailureHandling.STOP_ON_FAILURE)
	}

	@And("User stay in Diminati page")
	public void user_stay_in_Diminati_page() {
		WebUI.callTestCase(findTestCase('Pages/Page Daftar Jual Saya/Click Diminati Page'), [:], FailureHandling.STOP_ON_FAILURE)
	}

	@Then("User come to Detail Product page")
	public void user_come_to_Detail_Product_page() {
		WebUI.callTestCase(findTestCase('Pages/Page Daftar Jual Saya/Page Diminati/Go to the Detail Produk - Batalkan Transaksi'), [:], FailureHandling.STOP_ON_FAILURE)
	}

	@And("User click button Terima")
	public void user_click_button_Terima() {
		WebUI.callTestCase(findTestCase('Pages/Page Daftar Jual Saya/Page Diminati/Click Terima Tawaran'), [:], FailureHandling.STOP_ON_FAILURE)
	}

	@And("User change the status from the offer")
	public void user_change_the_status_from_the_offer() {
		WebUI.callTestCase(findTestCase('Pages/Page Daftar Jual Saya/Page Diminati/Click Button Status and Click Batalkan Transaksi'), [:], FailureHandling.STOP_ON_FAILURE)
	}
}



