package stepDefinition
import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testcase.TestCaseFactory
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testdata.TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI

import internal.GlobalVariable

import org.openqa.selenium.WebElement
import org.openqa.selenium.WebDriver
import org.openqa.selenium.By

import com.kms.katalon.core.mobile.keyword.internal.MobileDriverFactory
import com.kms.katalon.core.webui.driver.DriverFactory

import com.kms.katalon.core.testobject.RequestObject
import com.kms.katalon.core.testobject.ResponseObject
import com.kms.katalon.core.testobject.ConditionType
import com.kms.katalon.core.testobject.TestObjectProperty

import com.kms.katalon.core.mobile.helper.MobileElementCommonHelper
import com.kms.katalon.core.util.KeywordUtil

import com.kms.katalon.core.webui.exception.WebElementNotFoundException

import cucumber.api.java.en.And
import cucumber.api.java.en.Given
import cucumber.api.java.en.Then
import cucumber.api.java.en.When



class Search {
	@Given("user access to Secondhand website")
	public void user_access_to_Secondhand_website() {
		WebUI.callTestCase(findTestCase('Pages/Search/Navigate to Homepage'), [:], FailureHandling.STOP_ON_FAILURE)
	}

	@When("the Homepage displayed")
	public void the_Homepage_displayed() {
		WebUI.callTestCase(findTestCase('Pages/Search/Navigate to Homepage'), [:], FailureHandling.STOP_ON_FAILURE)
	}

	@Then("user click the Semua button {string}")
	public void user_click_the_Semua_button(String string) {
		WebUI.callTestCase(findTestCase('Pages/Search/Click button search by Semua'), [:], FailureHandling.STOP_ON_FAILURE)
	}

	@Then("user click the Hoby button {string}")
	public void user_click_the_Hoby_button(String string) {
		WebUI.callTestCase(findTestCase('Pages/Search/Click button search by Hoby'), [:], FailureHandling.STOP_ON_FAILURE)
	}

	@Then("user click the Kendaraan button {string}")
	public void user_click_the_Kendaraan_button(String string) {
		WebUI.callTestCase(findTestCase('Pages/Search/Click button search by Kendaraan'), [:], FailureHandling.STOP_ON_FAILURE)
	}

	@Then("user click the Elektronik button {string}")
	public void user_click_the_Elektronik_button(String string) {
		WebUI.callTestCase(findTestCase('Pages/Search/Click button search by Elektronik'), [:], FailureHandling.STOP_ON_FAILURE)
	}

	@Then("user click the Baju button {string}")
	public void user_click_the_Baju_button(String string) {
		WebUI.callTestCase(findTestCase('Pages/Search/Click button search by Baju'), [:], FailureHandling.STOP_ON_FAILURE)
	}
}