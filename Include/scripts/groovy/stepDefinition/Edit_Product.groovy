package stepDefinition
import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testcase.TestCaseFactory
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testdata.TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI

import internal.GlobalVariable

import org.openqa.selenium.WebElement
import org.openqa.selenium.WebDriver
import org.openqa.selenium.By

import com.kms.katalon.core.mobile.keyword.internal.MobileDriverFactory
import com.kms.katalon.core.webui.driver.DriverFactory

import com.kms.katalon.core.testobject.RequestObject
import com.kms.katalon.core.testobject.ResponseObject
import com.kms.katalon.core.testobject.ConditionType
import com.kms.katalon.core.testobject.TestObjectProperty

import com.kms.katalon.core.mobile.helper.MobileElementCommonHelper
import com.kms.katalon.core.util.KeywordUtil

import com.kms.katalon.core.webui.exception.WebElementNotFoundException

import cucumber.api.java.en.And
import cucumber.api.java.en.Given
import cucumber.api.java.en.Then
import cucumber.api.java.en.When



class Edit_Product {
	@Given("User go to the Daftar Jual page")
	public void user_go_to_the_Daftar_Jual_page() {
		WebUI.callTestCase(findTestCase('Pages/Homepage/Click Logo'), [:], FailureHandling.STOP_ON_FAILURE)

		WebUI.callTestCase(findTestCase('Pages/Homepage/Click Button Icon Daftar Jual'), [:], FailureHandling.STOP_ON_FAILURE)
	}

	@When("User is in the Semua Produk page")
	public void user_is_in_the_Semua_Produk_page() {
		WebUI.callTestCase(findTestCase('Pages/Page Daftar Jual Saya/Click Semua Produk Page'), [:], FailureHandling.STOP_ON_FAILURE)

		WebUI.callTestCase(findTestCase('Pages/Page Daftar Jual Saya/Verify Element Daftar Jual'), [:], FailureHandling.STOP_ON_FAILURE)
	}

	@And("User goes to Detail Product page")
	public void user_goes_to_Detail_Product_page() {
		WebUI.callTestCase(findTestCase('Pages/Page Daftar Jual Saya/_Page Semua Product/Click Detail Product - Semua Produk'),
				[:], FailureHandling.STOP_ON_FAILURE)
	}

	@Then("User clicks edit button")
	public void user_clicks_edit_button() {
		WebUI.callTestCase(findTestCase('Pages/Page Product Info/Click Button Edit Produk'), [:], FailureHandling.STOP_ON_FAILURE)
	}

	@And("User input Nama Produk")
	public void user_input_Nama_Produk() {
		WebUI.callTestCase(findTestCase('Pages/Page Product Info/Input Nama Produk'), [:], FailureHandling.STOP_ON_FAILURE)
	}

	@And("User drop Harga Produk")
	public void user_drop_Harga_Produk() {
		WebUI.callTestCase(findTestCase('Pages/Page Product Info/Input Harga Produk'), [:], FailureHandling.STOP_ON_FAILURE)
	}

	@And("User choose Category")
	public void user_choose_Category() {
		WebUI.callTestCase(findTestCase('Pages/Page Product Info/Choose Category'), [:], FailureHandling.STOP_ON_FAILURE)
	}

	@And("User input text Deskripsi")
	public void user_input_text_Deskripsi() {
		WebUI.callTestCase(findTestCase('Pages/Page Product Info/Input Deskripsi'), [:], FailureHandling.STOP_ON_FAILURE)
	}

	@Then("User clicks the Terbitkan button")
	public void user_clicks_the_Terbitkan_button() {
		WebUI.callTestCase(findTestCase('Pages/Page Product Info/Click Button Terbitkan'), [:], FailureHandling.STOP_ON_FAILURE)
	}
}