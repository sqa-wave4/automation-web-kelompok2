package stepDefinition

import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows

import cucumber.api.java.en.Given
import cucumber.api.java.en.Then
import cucumber.api.java.en.When
import internal.GlobalVariable

public class Register {

	@When("User click link daftar disini")
	public void user_click_link_daftar_disini() {
		WebUI.callTestCase(findTestCase('Pages/Page Login/Click Link Daftar Disini'), [:], FailureHandling.STOP_ON_FAILURE)
		WebUI.callTestCase(findTestCase('Pages/Page Register/Verify Element Register'), [:], FailureHandling.STOP_ON_FAILURE)
		WebUI.callTestCase(findTestCase('Pages/Page Register/Clear Text Nama, Email, and Password'), [:], FailureHandling.STOP_ON_FAILURE)
	}

	@When("User input nama lengkap")
	public void user_input_nama_lengkap() {
		WebUI.callTestCase(findTestCase('Pages/Page Register/Input Nama Lengkap'), [('NamaLengkap') : 'Kim Jennie'], FailureHandling.STOP_ON_FAILURE)
	}


	@When("User click button daftar")
	public void user_click_button_daftar() {
		WebUI.callTestCase(findTestCase('Pages/Page Register/Click Button Daftar'), [:], FailureHandling.STOP_ON_FAILURE)
	}

	@Then("Register success")
	public void register_success() {
		WebUI.callTestCase(findTestCase('Pages/Page Register/RA Successfully Register'), [('expectedText') : 'Silahkan verifikasi email agar dapat menggunakan layanan kami'],
		FailureHandling.STOP_ON_FAILURE)
	}

	@Then("Register failed error email already registered")
	public void register_failed_error_email_already_registered() {
		WebUI.callTestCase(findTestCase('Pages/Page Register/REM Failed Email Already Registered'), [('expectedText') : 'Email sudah digunakan'],
		FailureHandling.STOP_ON_FAILURE)
	}
}
