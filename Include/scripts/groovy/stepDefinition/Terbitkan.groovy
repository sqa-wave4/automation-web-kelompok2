package stepDefinition

import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows

import internal.GlobalVariable

import cucumber.api.java.en.And
import cucumber.api.java.en.Given
import cucumber.api.java.en.Then
import cucumber.api.java.en.When

public class Terbitkan {

	@Given("User go to Daftar Jual Saya page")
	public void user_already_on_Daftar_Jual_Saya_page() {
		WebUI.callTestCase(findTestCase('Pages/Homepage/Navigate to Daftar Jual Saya Page'), [:], FailureHandling.STOP_ON_FAILURE)
	}

	@When("User already select product")
	public void user_already_select_product() {
		WebUI.callTestCase(findTestCase('Pages/Page Terbitkan Produk/Go to the Detail Produk page'), [:], FailureHandling.STOP_ON_FAILURE)
	}

	@Then("User click button Terbitkan")
	public void user_click_button_Terbitkan() {
		WebUI.callTestCase(findTestCase('Pages/Page Terbitkan Produk/Click Button Terbitkan'), [:], FailureHandling.STOP_ON_FAILURE)
	}
}