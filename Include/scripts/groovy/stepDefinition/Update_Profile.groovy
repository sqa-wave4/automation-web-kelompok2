package stepDefinition

import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows

import cucumber.api.java.en.And
import cucumber.api.java.en.Given
import cucumber.api.java.en.Then
import cucumber.api.java.en.When

import internal.GlobalVariable

public class Update_Profile {
	@Given("User access dashboard page")
	public void user_access_dashboard_page() {
	}

	@When("User click profile icon")
	public void user_click_profile_icon() {
		WebUI.callTestCase(findTestCase('Pages/Page Profile/Click Menu Profile'), [:], FailureHandling.STOP_ON_FAILURE)
	}

	@And("User click “profile” in dropdown")
	public void user_click_profile_in_dropdown() {
		WebUI.callTestCase(findTestCase('Pages/Page Profile/Click Sub Menu Profile'), [:], FailureHandling.STOP_ON_FAILURE)
	}

	@And("User input all the form fields")
	public void user_input_all_the_form_fields() {

		WebUI.callTestCase(findTestCase('Pages/Page Profile/Input Nama'), [:], FailureHandling.STOP_ON_FAILURE)

		WebUI.callTestCase(findTestCase('Pages/Page Profile/Input Kota'), [:], FailureHandling.STOP_ON_FAILURE)

		WebUI.callTestCase(findTestCase('Pages/Page Profile/Input Alamat'), [:], FailureHandling.STOP_ON_FAILURE)
	}

	@And("Click {string}")
	public void click(String string) {
		WebUI.callTestCase(findTestCase('Pages/Page Profile/Click Button Submit'), [:], FailureHandling.STOP_ON_FAILURE)
	}

	@Then("Success to update profile. The System shows alert “Berhasil Update Profile”")
	public void success_to_update_profile_The_System_shows_alert_Berhasil_Update_Profile() {
		WebUI.callTestCase(findTestCase('Pages/Page Profile/Alert Success Update Profile'), [('expectedText') : 'Berhasil update profile'],
		FailureHandling.STOP_ON_FAILURE)
	}
}
