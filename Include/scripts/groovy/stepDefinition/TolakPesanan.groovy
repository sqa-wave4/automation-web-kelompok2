package stepDefinition

import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows

import internal.GlobalVariable

import cucumber.api.java.en.And
import cucumber.api.java.en.Given
import cucumber.api.java.en.Then
import cucumber.api.java.en.When

public class TolakPesanan {

	@Given("user stays on the page of Diminati")
	public void user_stays_on_the_page_of_Diminati() {
		WebUI.callTestCase(findTestCase('Pages/Page Daftar Jual Saya/Click Diminati Page'), [:], FailureHandling.STOP_ON_FAILURE)
	}

	@When("User pick a product")
	public void user_pick_a_product() {
		WebUI.callTestCase(findTestCase('Pages/Page Daftar Jual Saya/Page Diminati/Go to the Detail Produk - Tolak'), [:], FailureHandling.STOP_ON_FAILURE)
	}

	@Then("User click button Tolak")
	public void user_click_button_Tolak() {
		WebUI.callTestCase(findTestCase('Pages/Page Daftar Jual Saya/Page Diminati/Click Tolak Tawaran'), [:], FailureHandling.STOP_ON_FAILURE)
	}
	
	@And("Wait for Confirmation Alert")
	public void Wait_for_Confirmation_Alert() {
		WebUI.callTestCase(findTestCase('Pages/Page Daftar Jual Saya/_Page Diminati/RA Tawaran Produk Ditolak'), [('expectedText') : 'Tawaran produk ditolak'],
			FailureHandling.STOP_ON_FAILURE)
	}
}
