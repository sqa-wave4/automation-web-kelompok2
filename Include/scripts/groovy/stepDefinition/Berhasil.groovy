package stepDefinition

import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows

import internal.GlobalVariable

import cucumber.api.java.en.And
import cucumber.api.java.en.Given
import cucumber.api.java.en.Then
import cucumber.api.java.en.When

public class Berhasil {
	
	@Given("user is on the page of Diminati page")
	public void user_is_on_the_page_of_Diminati_page() {
		WebUI.callTestCase(findTestCase('Pages/Page Daftar Jual Saya/Click Diminati Page'), [:], FailureHandling.STOP_ON_FAILURE)
	}
	
	@When("user choose a product")
	public void user_choose_a_product() {
		WebUI.callTestCase(findTestCase('Pages/Page Daftar Jual Saya/Page Diminati/Go to the Detail Produk - Berhasil Terjual'), [:], FailureHandling.STOP_ON_FAILURE)
	}
	
	@Then("user clicks the Terima button")
	public void user_clicks_the_Terima_button() {
		WebUI.callTestCase(findTestCase('Pages/Page Daftar Jual Saya/Page Diminati/Click Terima Tawaran'), [:], FailureHandling.STOP_ON_FAILURE)
	}
	
	@Then("User clicks the Berhasil Terjual button")
	public void user_clicks_the_Berhasil_Terjual_button() {
		WebUI.callTestCase(findTestCase('Pages/Page Daftar Jual Saya/Page Diminati/Click Button Status and Click Berhasil Terjual'), [:], FailureHandling.STOP_ON_FAILURE)
	}
}