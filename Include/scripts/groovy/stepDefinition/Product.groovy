package stepDefinition

import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows

import cucumber.api.java.en.Given
import cucumber.api.java.en.Then
import cucumber.api.java.en.When
import internal.GlobalVariable

public class Product {

	@Given("User already on daftar jual page")
	public void user_already_on_daftar_jual_page() {
		WebUI.callTestCase(findTestCase('Pages/Homepage/Click Logo'), [:], FailureHandling.STOP_ON_FAILURE)
		WebUI.callTestCase(findTestCase('Pages/Homepage/Click Button Icon Daftar Jual'), [:], FailureHandling.STOP_ON_FAILURE)
		WebUI.callTestCase(findTestCase('Pages/Page Daftar Jual Saya/Verify Element Daftar Jual'), [:], FailureHandling.STOP_ON_FAILURE)
	}

	@When("User click tambah produk")
	public void user_click_tambah_produk() {
		WebUI.callTestCase(findTestCase('Pages/Page Daftar Jual Saya/_Page Semua Product/Click Button Tambah Produk'), [:], FailureHandling.STOP_ON_FAILURE)
		WebUI.callTestCase(findTestCase('Pages/Page Product Info/Verify Element Product Info'), [:], FailureHandling.STOP_ON_FAILURE)
	}

	@When("User input nama produk")
	public void user_input_nama_produk() {
		WebUI.callTestCase(findTestCase('Pages/Page Product Info/Input Nama Produk'), [:], FailureHandling.STOP_ON_FAILURE)
	}

	@When("User input harga produk")
	public void user_input_harga_produk() {

		WebUI.callTestCase(findTestCase('Pages/Page Product Info/Input Harga Produk'), [:], FailureHandling.STOP_ON_FAILURE)
	}

	@When("User choose category produk")
	public void user_choose_category_produk() {

		WebUI.callTestCase(findTestCase('Pages/Page Product Info/Choose Category'), [:], FailureHandling.STOP_ON_FAILURE)
	}

	@When("User input deskripsi produk")
	public void user_input_deskripsi_produk() {

		WebUI.callTestCase(findTestCase('Pages/Page Product Info/Input Deskripsi'), [:], FailureHandling.STOP_ON_FAILURE)
	}

	@When("User click button terbitkan")
	public void user_click_button_terbitkan() {
		WebUI.callTestCase(findTestCase('Pages/Page Product Info/Click Button Terbitkan'), [:], FailureHandling.STOP_ON_FAILURE)
	}

	@When("User click button preview")
	public void user_click_button_preview() {
		WebUI.callTestCase(findTestCase('Pages/Page Product Info/Click Button Preview'), [:], FailureHandling.STOP_ON_FAILURE)
	}

	@Then("User success add product")
	public void user_success_add_product_by_terbitkan() {
		WebUI.callTestCase(findTestCase('Pages/Page Daftar Jual Saya/Verify Element Daftar Jual'), [:], FailureHandling.STOP_ON_FAILURE)
	}
}
