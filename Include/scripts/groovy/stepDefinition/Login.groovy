package stepDefinition
import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testcase.TestCaseFactory
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testdata.TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI

import internal.GlobalVariable

import org.openqa.selenium.WebElement
import org.openqa.selenium.WebDriver
import org.openqa.selenium.By

import com.kms.katalon.core.mobile.keyword.internal.MobileDriverFactory
import com.kms.katalon.core.webui.driver.DriverFactory

import com.kms.katalon.core.testobject.RequestObject
import com.kms.katalon.core.testobject.ResponseObject
import com.kms.katalon.core.testobject.ConditionType
import com.kms.katalon.core.testobject.TestObjectProperty

import com.kms.katalon.core.mobile.helper.MobileElementCommonHelper
import com.kms.katalon.core.util.KeywordUtil

import com.kms.katalon.core.webui.exception.WebElementNotFoundException

import cucumber.api.java.en.And
import cucumber.api.java.en.Given
import cucumber.api.java.en.Then
import cucumber.api.java.en.When


class Login {

	@Given("User already on login page")
	public void user_already_on_login_page() {
		WebUI.callTestCase(findTestCase('Pages/Page Login/Navigate to Login Page'), [:], FailureHandling.STOP_ON_FAILURE)
		WebUI.callTestCase(findTestCase('Pages/Page Login/Clear Text Email and Password'), [:], FailureHandling.STOP_ON_FAILURE)
	}


	@When("User input registered email")
	public void user_input_registered_email() {
		WebUI.callTestCase(findTestCase('Pages/Page Login/Input Email - Login'), [:], FailureHandling.STOP_ON_FAILURE)
	}

	@When("User input correct password")
	public void user_input_correct_password() {
		WebUI.callTestCase(findTestCase('Pages/Page Login/Input Password - Login'), [:], FailureHandling.STOP_ON_FAILURE)
	}

	@When("User click button masuk")
	public void user_click_button_masuk() {
		WebUI.callTestCase(findTestCase('Pages/Page Login/Click Button Masuk Login'), [:], FailureHandling.STOP_ON_FAILURE)
	}

	@When("User input incorrect password")
	public void user_input_incorrect_password() {
		WebUI.callTestCase(findTestCase('Pages/Page Login/Input Random Password - Login'), [:], FailureHandling.STOP_ON_FAILURE)
	}


	@When("User input unregistered email")
	public void user_input_unregistered_email() {
		WebUI.callTestCase(findTestCase('Pages/Page Login/Input Random Email - Login'), [:], FailureHandling.STOP_ON_FAILURE)
	}

	@Then("User successfully login")
	public void user_successfully_login() {
		WebUI.callTestCase(findTestCase('Pages/Homepage/Verify Logged-in'), [:], FailureHandling.STOP_ON_FAILURE)
	}

	@Then("User failed login error incorrect password")
	public void user_failed_login_error_incorrect_password() {
		WebUI.callTestCase(findTestCase('Pages/Page Login/REM Incorrect Password'), [:], FailureHandling.STOP_ON_FAILURE)
	}

	@Then("User failed login error unregistered account")
	public void user_failed_login_error_unregistered_account() {
		WebUI.callTestCase(findTestCase('Pages/Page Login/REM Unregistered Email'), [('expectedText') : 'Akun tidak ditemukan'],
		FailureHandling.STOP_ON_FAILURE)
	}
}