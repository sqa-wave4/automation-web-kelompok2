@Negotiation
Feature: negotiation the product

  @NE001
  Scenario: NE001 - User negotiating the product without the price
    Given user choose the product
    When the product detail is displayed
    Then user click the Saya Tertarik button
    Then user click the Kirim button

  @NE002
  Scenario: NE002 - User negotiating the product without the price
    Given user choose the product
    When the product detail is displayed
    Then user click the Saya Tertarik button
    Then user input the price "input_hargatawar"
    Then user click the Kirim button