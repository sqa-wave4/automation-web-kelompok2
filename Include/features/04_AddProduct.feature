@AddProduct
Feature: AddProduct

  @ADD001
  Scenario: User want to add product by terbitkan button
    Given User already on daftar jual page
    When User click tambah produk
    And User input nama produk
    And User input harga produk
    And User choose category produk
    And User input deskripsi produk
    And User click button terbitkan
    Then User success add product

  @ADD001
  Scenario: User want to add product by terbitkan button
    Given User already on daftar jual page
    When User click tambah produk
    And User input nama produk
    And User input harga produk
    And User choose category produk
    And User input deskripsi produk
    And User click button preview
    Then User success add product
