@Register
Feature: Register

  @RGI001
  Scenario: User want to register with valid credential
    Given User already on login page
    When User click link daftar disini
    And User input nama lengkap
    And User input unregistered email
    And User input correct password
    And User click button daftar
    Then Register success

  @RGI002
  Scenario: User want to register with registered email
    Given User already on login page
    When User click link daftar disini
    And User input nama lengkap
    And User input registered email
    And User input correct password
    And User click button daftar
    Then Register failed error email already registered
