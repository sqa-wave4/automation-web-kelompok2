@Login
Feature: Login

  @LGI001
  Scenario: User want to login using registered email and correct password
    Given User already on login page
    When User input registered email
    And User input correct password
    And User click button masuk
    Then User successfully login

  @LGI002
  Scenario: User want to login using registered email and incorrect password
    Given User already on login page
    When User input registered email
    And User input incorrect password
    And User click button masuk
    Then User failed login error incorrect password

  @LGI003
  Scenario: User want to login using unregistered email
    Given User already on login page
    When User input unregistered email
    And User input incorrect password
    And User click button masuk
    Then User failed login error unregistered account
