@Logout
Feature: Logout

  @LGO001
  Scenario: User want to logout
    Given user click button icon profile
    When user click dropdown logout
    Then user logged out
