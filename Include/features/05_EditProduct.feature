@EditProduct
Feature: EditProduct

@EDT001
Scenario: User Change the Status to Berhasil Terjual
	Given User go to the Daftar Jual page
	When User is in the Semua Produk page
	And User goes to Detail Product page
	Then User clicks edit button
	And User input Nama Produk
	And User drop Harga Produk
	And User choose Category
	And User input text Deskripsi
	Then User clicks the Terbitkan button
