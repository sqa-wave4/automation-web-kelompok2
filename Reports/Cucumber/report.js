$(document).ready(function() {var formatter = new CucumberHTML.DOMFormatter($('.cucumber-report'));formatter.uri("Include/features/02_Login.feature");
formatter.feature({
  "name": "Login",
  "description": "",
  "keyword": "Feature",
  "tags": [
    {
      "name": "@Login"
    }
  ]
});
formatter.scenario({
  "name": "User want to login using registered email and correct password",
  "description": "",
  "keyword": "Scenario",
  "tags": [
    {
      "name": "@Login"
    },
    {
      "name": "@LGI001"
    }
  ]
});
formatter.step({
  "name": "User already on login page",
  "keyword": "Given "
});
formatter.match({
  "location": "Login.user_already_on_login_page()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User input registered email",
  "keyword": "When "
});
formatter.match({
  "location": "Login.user_input_registered_email()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User input correct password",
  "keyword": "And "
});
formatter.match({
  "location": "Login.user_input_correct_password()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User click button masuk",
  "keyword": "And "
});
formatter.match({
  "location": "Login.user_click_button_masuk()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User successfully login",
  "keyword": "Then "
});
formatter.match({
  "location": "Login.user_successfully_login()"
});
formatter.result({
  "status": "passed"
});
formatter.uri("Include/features/03_Logout.feature");
formatter.feature({
  "name": "Logout",
  "description": "",
  "keyword": "Feature",
  "tags": [
    {
      "name": "@Logout"
    }
  ]
});
formatter.scenario({
  "name": "User want to logout",
  "description": "",
  "keyword": "Scenario",
  "tags": [
    {
      "name": "@Logout"
    },
    {
      "name": "@LGO001"
    }
  ]
});
formatter.step({
  "name": "user click button icon profile",
  "keyword": "Given "
});
formatter.match({
  "location": "Logout.user_click_button_icon_profile()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "user click dropdown logout",
  "keyword": "When "
});
formatter.match({
  "location": "Logout.user_click_dropdown_logout()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "user logged out",
  "keyword": "Then "
});
formatter.match({
  "location": "Logout.user_logged_out()"
});
formatter.result({
  "status": "passed"
});
});