<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>warn_AkunTidakDitemukan</name>
   <tag></tag>
   <elementGuidId>0ac4bcb6-1023-4707-a868-c120b7d1f14a</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>p</value>
      </entry>
      <entry>
         <key>BASIC</key>
         <value>//*[(text() = 'Akun tidak ditemukan' or . = 'Akun tidak ditemukan')]</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='root']/div/div/div[2]/form/div/p</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//div[@class='alert alert-danger alert-dismissible fade show']</value>
      <webElementGuid>35e73ea1-c919-4137-a1c0-816a1797fcb3</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Akun tidak ditemukan</value>
      <webElementGuid>5448cc06-6826-4411-a84b-21a94d31cbd7</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='root']/div/div/div[2]/form/div/p</value>
      <webElementGuid>15f1b2a5-0f8b-424e-b26d-53cce443400e</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Masuk'])[1]/following::p[1]</value>
      <webElementGuid>921746c9-c6b0-449c-a62f-14e45921e249</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='SecondHand.'])[1]/following::p[1]</value>
      <webElementGuid>61ec7d43-e627-4200-b24c-3823c40ad78f</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Email'])[1]/preceding::p[1]</value>
      <webElementGuid>a034b6a3-6062-4e9d-a7cf-da31395843a5</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Password'])[1]/preceding::p[1]</value>
      <webElementGuid>df7ea12f-8bd6-4cd5-a7c4-494e0005e02e</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Password anda salah!']/parent::*</value>
      <webElementGuid>ed407fd0-7835-4899-b5b4-2e0333864e52</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//p</value>
      <webElementGuid>aedfa59f-c011-42e4-8cfc-3f5867cba557</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//p[(text() = 'Password anda salah!' or . = 'Password anda salah!')]</value>
      <webElementGuid>d6895df7-148b-4351-9337-216e08d61916</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
