<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>field_Password_Login</name>
   <tag></tag>
   <elementGuidId>fa74a86f-81b1-4e8b-a52d-794e36833747</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//input[@id='exampleInputPassword1 ']</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>[placeholder='Masukkan password']</value>
      </entry>
      <entry>
         <key>BASIC</key>
         <value>id(&quot;exampleInputPassword1 &quot;)</value>
      </entry>
   </selectorCollection>
   <selectorMethod>CSS</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>input</value>
      <webElementGuid>b746ee4c-b8e8-42a1-8640-165cdb28e7b4</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>type</name>
      <type>Main</type>
      <value>password</value>
      <webElementGuid>a335c032-ef0c-4703-92a7-c5beddbefba5</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>form-control </value>
      <webElementGuid>e09088cc-c1ca-46ef-966a-733c64bbb059</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>exampleInputPassword1 </value>
      <webElementGuid>0fbff21e-a970-42ba-8abf-a240103afa29</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>placeholder</name>
      <type>Main</type>
      <value>Masukkan password</value>
      <webElementGuid>19041664-48d7-4a1a-90a9-c4f1f2d8842b</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;exampleInputPassword1 &quot;)</value>
      <webElementGuid>0d2e6cd3-36be-41d7-b7d5-8a72d514218d</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//input[@id='exampleInputPassword1 ']</value>
      <webElementGuid>c47b0dc6-5019-49fc-b315-5c5f236fb577</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='root']/div/div/div[2]/form/div[2]/input</value>
      <webElementGuid>eb8a4feb-e301-4f96-83e1-2adc448ecc93</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[2]/input</value>
      <webElementGuid>72002c89-7d3b-4e3a-bb46-2fe5a1ae23df</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//input[@type = 'password' and @id = 'exampleInputPassword1 ' and @placeholder = 'Masukkan password']</value>
      <webElementGuid>70168ac8-aeb7-44f1-8f9d-1392faedd2a5</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
