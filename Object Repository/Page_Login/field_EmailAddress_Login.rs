<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>field_EmailAddress_Login</name>
   <tag></tag>
   <elementGuidId>2f67ec9c-82d1-48f2-85a7-c973e80e55ed</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//input[@id='exampleInputEmail1']</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>[placeholder='Contoh: johndee@gmail.com']</value>
      </entry>
      <entry>
         <key>BASIC</key>
         <value>id(&quot;exampleInputEmail1&quot;)[count(. | //*[@type = 'email' and @id = 'exampleInputEmail1']) = count(//*[@type = 'email' and @id = 'exampleInputEmail1'])]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>CSS</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>input</value>
      <webElementGuid>6342357a-a006-4d4d-ae83-a2c85ef96719</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>type</name>
      <type>Main</type>
      <value>email</value>
      <webElementGuid>70b37046-c7fe-4d2a-aff9-1a452661387c</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>form-control</value>
      <webElementGuid>d0998bf0-d923-40c2-9b33-a742c2b24ea2</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>exampleInputEmail1</value>
      <webElementGuid>d41ab9f3-ac29-4a48-b6bf-9c096726fc7e</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>placeholder</name>
      <type>Main</type>
      <value>Contoh: johndee@gmail.com</value>
      <webElementGuid>07b24eb9-eac9-4f91-a631-d0d1a3c4193a</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;exampleInputEmail1&quot;)</value>
      <webElementGuid>365926f7-0dfe-4780-8654-46851f897712</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//input[@id='exampleInputEmail1']</value>
      <webElementGuid>f65e0e09-521a-4d9e-9fe9-4dea37366ce3</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='root']/div/div/div[2]/form/div/input</value>
      <webElementGuid>47ae4792-d134-45bd-9bfe-801248f443bb</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//input</value>
      <webElementGuid>d4519f60-eaee-49dc-9b07-2381d72a6394</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//input[@type = 'email' and @id = 'exampleInputEmail1' and @placeholder = 'Contoh: johndee@gmail.com']</value>
      <webElementGuid>7af233e6-225f-4d7f-a329-a34ae4d99485</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
