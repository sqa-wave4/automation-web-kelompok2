<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description>Dropdown Logout</description>
   <name>drdwn_logout</name>
   <tag></tag>
   <elementGuidId>4df9b401-02fa-4c1b-badc-c018442c8ad1</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>.btn.dropdown-item</value>
      </entry>
      <entry>
         <key>BASIC</key>
         <value>//button[@class='dropdown-item btn']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//button[@class='dropdown-item btn']</value>
      <webElementGuid>8b9bd37a-6c3d-4780-bdd4-4347591e0fc3</webElementGuid>
   </webElementProperties>
</WebElementEntity>
