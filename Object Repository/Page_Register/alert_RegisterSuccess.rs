<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description>Alert appear : Silahkan verifikasi email agar dapat menggunakan layanan kami</description>
   <name>alert_RegisterSuccess</name>
   <tag></tag>
   <elementGuidId>23482bfb-3893-4f74-a492-1539a94132cd</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[(text() = 'Silahkan verifikasi email agar dapat menggunakan layanan kami' or . = 'Silahkan verifikasi email agar dapat menggunakan layanan kami')]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Silahkan verifikasi email agar dapat menggunakan layanan kami</value>
      <webElementGuid>6a46e3e5-8331-49b7-aebd-cb1e64d48fe1</webElementGuid>
   </webElementProperties>
</WebElementEntity>
