<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description>text : Sudah Punya Akun?</description>
   <name>txt_SudahPunyaAkun</name>
   <tag></tag>
   <elementGuidId>7a56968f-3b63-48c3-9837-b052f4a45525</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[(text() = 'Sudah punya akun?' or . = 'Sudah punya akun?')]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Sudah punya akun?</value>
      <webElementGuid>dbbfd0e4-39f1-44e6-9979-baf96374a383</webElementGuid>
   </webElementProperties>
</WebElementEntity>
