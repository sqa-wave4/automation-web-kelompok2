<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description>Warning : Email sudah digunakan</description>
   <name>warn_EmailAlreadyRegistered</name>
   <tag></tag>
   <elementGuidId>326ea2e5-3ca9-41bb-8d36-2cef2871e99c</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[(text() = 'Email sudah digunakan' or . = 'Email sudah digunakan')]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Email sudah digunakan</value>
      <webElementGuid>e1afd959-5d78-4d27-9d20-4c3026a2fa48</webElementGuid>
   </webElementProperties>
</WebElementEntity>
