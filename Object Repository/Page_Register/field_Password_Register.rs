<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>field_Password_Register</name>
   <tag></tag>
   <elementGuidId>99d1f14f-696b-4831-83e2-105319000fdc</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>[placeholder='Masukkan password']</value>
      </entry>
      <entry>
         <key>BASIC</key>
         <value>id(&quot;exampleInputPassword1 &quot;)[count(. | //*[@type = 'password' and @id = 'exampleInputPassword1 ']) = count(//*[@type = 'password' and @id = 'exampleInputPassword1 '])]</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//input[@id='exampleInputPassword1 ']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>CSS</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>input</value>
      <webElementGuid>b290d957-c7a9-4264-9e8b-f2287e0db830</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>type</name>
      <type>Main</type>
      <value>password</value>
      <webElementGuid>761a47d7-56cc-4f2e-b14c-0e438e0e3818</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>form-control </value>
      <webElementGuid>49b8493e-4237-42c1-a239-b1e57f472715</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>exampleInputPassword1 </value>
      <webElementGuid>c3e0ca6a-93f1-4532-bf3b-4a890df912a6</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>placeholder</name>
      <type>Main</type>
      <value>Masukkan password</value>
      <webElementGuid>a01e8092-f3c7-481b-8c1c-3b40e8ad097c</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;exampleInputPassword1 &quot;)</value>
      <webElementGuid>0927eb63-c85b-40b4-a275-5724625d9325</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//input[@id='exampleInputPassword1 ']</value>
      <webElementGuid>adf177d2-0e27-4a56-a72d-cb1ef3a49cf0</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='root']/div/div/div[2]/form/div[3]/input</value>
      <webElementGuid>1ced9658-12fd-4c78-b38b-051b58f3400a</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[3]/input</value>
      <webElementGuid>a9403874-eb0b-4ed5-bf14-9d7a75c795a3</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//input[@type = 'password' and @id = 'exampleInputPassword1 ' and @placeholder = 'Masukkan password']</value>
      <webElementGuid>e87bbe30-a778-47f0-93ce-659e9050ca8a</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
