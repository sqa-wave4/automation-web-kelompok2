<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>warn_HargaTerkirim</name>
   <tag></tag>
   <elementGuidId>ae4838df-4da6-4c21-904b-cd3a38289b24</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//div[@class='fade position-absolute top-0 start-50 translate-middle alert alert-success show']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//div[@class='fade position-absolute top-0 start-50 translate-middle alert alert-success show']</value>
      <webElementGuid>274e4ef0-ef58-4c73-ab03-9dc782311082</webElementGuid>
   </webElementProperties>
</WebElementEntity>
