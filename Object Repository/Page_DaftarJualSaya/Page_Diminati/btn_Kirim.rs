<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>btn_Kirim</name>
   <tag></tag>
   <elementGuidId>9df9b585-029f-4281-a027-d772dd898ee4</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//button[@class='btn btn-primary']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//button[@class='btn btn-primary']</value>
      <webElementGuid>6715b778-fc41-487b-a007-0d761b5e2ff2</webElementGuid>
   </webElementProperties>
</WebElementEntity>
