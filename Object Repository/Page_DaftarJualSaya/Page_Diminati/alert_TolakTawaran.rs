<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description>Alert : Tawaran Produk Ditolak</description>
   <name>alert_TolakTawaran</name>
   <tag></tag>
   <elementGuidId>6f9ad152-d393-4cd8-8b73-4213706df255</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//div[@class='fade position-absolute top-0 start-50 translate-middle alert alert-danger show']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//div[@class='fade position-absolute top-0 start-50 translate-middle alert alert-danger show']</value>
      <webElementGuid>adfdda19-56a9-4a3e-9b0a-76511edb6ac3</webElementGuid>
   </webElementProperties>
</WebElementEntity>
