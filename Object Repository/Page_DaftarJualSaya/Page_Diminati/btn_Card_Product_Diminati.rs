<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>btn_Card_Product_Diminati</name>
   <tag></tag>
   <elementGuidId>e4081a7a-74b2-4eec-bcf5-15f3811fc8b2</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//div[@class='row row-cols-lg-3']/div[3]//div[@class='card-body']</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>.content</value>
      </entry>
   </selectorCollection>
   <selectorMethod>CSS</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//div[@class='row row-cols-lg-3']/div[3]//div[@class='card-body']</value>
      <webElementGuid>12e94868-b930-422b-b5eb-e1c791eaf41c</webElementGuid>
   </webElementProperties>
</WebElementEntity>
