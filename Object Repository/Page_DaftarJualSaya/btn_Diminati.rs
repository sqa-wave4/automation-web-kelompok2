<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>btn_Diminati</name>
   <tag></tag>
   <elementGuidId>5b2835f2-4d7b-40d8-a2e5-d739a8ea4955</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>button:nth-of-type(2) > [type='button']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>css</name>
      <type>Main</type>
      <value>button:nth-of-type(2) > [type='button']</value>
      <webElementGuid>af640887-27db-406e-a101-71cb632c1358</webElementGuid>
   </webElementProperties>
</WebElementEntity>
