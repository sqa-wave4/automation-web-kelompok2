<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>btn_Tambah_Produk</name>
   <tag></tag>
   <elementGuidId>0d8120fe-a817-4bd9-8b12-cf3888101cd5</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//a[@type='button']</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>a.card.content.add</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>a</value>
      <webElementGuid>40988c2b-5e63-42fd-8d24-1d149a99e34a</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>card content add</value>
      <webElementGuid>4dae4a82-9f52-4a7c-8070-6fcb34fbb114</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>type</name>
      <type>Main</type>
      <value>button</value>
      <webElementGuid>4d47f5b3-2839-4b91-8bd9-72d93a458e81</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>href</name>
      <type>Main</type>
      <value>/productinfo</value>
      <webElementGuid>03d66096-3007-4e64-a8bf-8ae41e123410</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value> Tambah Produk</value>
      <webElementGuid>4707f906-c7a1-409f-ae0e-c2b0549c9bd9</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;root&quot;)/div[1]/div[@class=&quot;mt-4 pb-4 container&quot;]/div[@class=&quot;mt-4 row&quot;]/div[@class=&quot;col-lg-9 col&quot;]/div[@class=&quot;row row-cols-lg-3&quot;]/div[@class=&quot;col&quot;]/a[@class=&quot;card content add&quot;]</value>
      <webElementGuid>4b762698-19ea-4a02-813f-0f0c9a84f228</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//a[@type='button']</value>
      <webElementGuid>e46252e7-e137-41d9-84ea-4529bd786e08</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='root']/div/div/div[3]/div[2]/div/div/a</value>
      <webElementGuid>dbdea384-728b-4e5e-b1d8-5ddc8dcdb2b3</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Capture object:'])[1]/preceding::a[1]</value>
      <webElementGuid>4b14a921-6676-415e-b321-1a02f586d892</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Alt'])[1]/preceding::a[1]</value>
      <webElementGuid>ef4cf5f2-13fe-445b-b66c-67d294c35282</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:href</name>
      <type>Main</type>
      <value>//a[contains(@href, '/productinfo')]</value>
      <webElementGuid>3d3af4bc-82cb-495f-9fcf-bcd921a025e2</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[3]/div[2]/div/div/a</value>
      <webElementGuid>7298f09f-c7b9-4981-a15d-7ae469357c19</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//a[@type = 'button' and @href = '/productinfo' and (text() = ' Tambah Produk' or . = ' Tambah Produk')]</value>
      <webElementGuid>561dfaac-608b-487a-a617-af7bff313b6e</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
